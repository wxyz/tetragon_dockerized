(function ($, Drupal, CKEDITOR) {
  'use strict';

  CKEDITOR.plugins.add('imageteaser', {
    requires: 'widget',
    init: function (editor) {
      editor.addContentsCss(this.path + 'css/style.css');

      editor.widgets.add('imageteaserleft', {
        button: 'Image with text on the right side.',
        template:
          '<div class="wysiwyg-content wysiwyg-content--image-on-left-with-text">' +
            '<div class="wysiwyg-content__image"><img src="' + this.path + 'images/image-placeholder.png" /></div>' +
            '<div class="wysiwyg-content__text"><p>Content</p></div>' +
          '</div>',
        editables: {
          image: {
            selector: '.wysiwyg-content__image',
            allowedContent: 'img[*]'
          },
          content: {
            selector: '.wysiwyg-content__text',
            allowedContent: 'p br ul ol li strong em h2'
          }
        },
        allowedContent: 'div(!wysiwyg-content); div(!wysiwyg-content__image); img(!wysiwyg-content__text)',
        requiredContent: 'div(wysiwyg-content)',
        upcast: function (element) {
          return element.name === 'div' && element.hasClass('wysiwyg-content--image-on-left-with-text');
        }
      });

      editor.widgets.add('imageteaserright', {
        button: 'Image with text on the left side.',
        template:
        '<div class="wysiwyg-content wysiwyg-content--image-on-right-with-text">' +
          '<div class="wysiwyg-content__image"><img src="' + this.path + 'images/image-placeholder.png" /></div>' +
          '<div class="wysiwyg-content__text"><p>Content</p></div>' +
        '</div>',
      editables: {
        image: {
          selector: '.wysiwyg-content__image',
          allowedContent: 'img[*]'
        },
        content: {
          selector: '.wysiwyg-content__text',
          allowedContent: 'p br ul ol li strong em h2'
        }
      },
      allowedContent: 'div(!wysiwyg-content); div(!wysiwyg-content__image); img(!wysiwyg-content__text)',
      requiredContent: 'div(wysiwyg-content)',
      upcast: function (element) {
        return element.name === 'div' && element.hasClass('wysiwyg-content--image-on-right-with-text');
        }
      });

      editor.widgets.add('imageteaserrightfull', {
        button: 'Image with title and text on the left side.',
        template:
          '<div class="wysiwyg-content wysiwyg-content--image-on-right-with-title-and-text">' +
            '<div class="wysiwyg-content__image"><img src="' + this.path + 'images/image-placeholder.png" /></div>' +
            '<h2 class="wysiwyg-content__title">Title</h2>' +
            '<div class="wysiwyg-content__text"><p>Content</p></div>' +
          '</div>',
        editables: {
          image: {
            selector: '.wysiwyg-content__image',
            allowedContent: 'img[*]'
          },
          title: {
            selector: '.wysiwyg-content__title',
            allowedContent: 'br strong em'
          },
          content: {
            selector: '.wysiwyg-content__text',
            allowedContent: 'p h2 br ul ol li strong em'
          }
        },
        allowedContent: 'div(!wysiwyg-content); h2(!wysiwyg-content__image); div(!wysiwyg-content__title); img(!wysiwyg-content__text)',
        requiredContent: 'div(wysiwyg-content)',
        upcast: function (element) {
          return element.name === 'div' && element.hasClass('wysiwyg-content--image-on-right-with-title-and-text');
        }
      });

      editor.widgets.add('doubleimageblock', {
        button: 'Double image block.',
        template:
          '<div class="wysiwyg-content wysiwyg-content--double-image">' +
            '<div class="wysiwyg-content__image wysiwyg-content__image--left"><img src="' + this.path + 'images/image-placeholder.png" /></div>' +
            '<div class="wysiwyg-content__image wysiwyg-content__image--right"><img src="' + this.path + 'images/image-placeholder.png" /></div>' +
          '</div>',
        editables: {
          leftImage: {
            selector: '.wysiwyg-content__image--left',
            allowedContent: 'img[*]'
          },
          rightImage: {
            selector: '.wysiwyg-content__image--right',
            allowedContent: 'img[*]'
          }
        },
        allowedContent: 'div(!wysiwyg-content); img(!wysiwyg-content__image)',
        requiredContent: 'div(imageblock)',
        upcast: function (element) {
          return element.name === 'div' && element.hasClass('wysiwyg-content--double-image');
        }
      });

      editor.widgets.add('doubletextblock', {
        button: 'Double text block.',
        template:
          '<div class="wysiwyg-content wysiwyg-content--double-text">' +
            '<div class="wysiwyg-content__text wysiwyg-content__text--left"><p>Content</p></div>' +
            '<div class="wysiwyg-content__text wysiwyg-content__text--right"><p>Content</p></div>' +
          '</div>',
        editables: {
          leftcontent: {
            selector: '.wysiwyg-content__text--left',
            allowedContent: 'p br h2 ul ol li strong em'
          },
          rightcontent: {
            selector: '.wysiwyg-content__text--right',
            allowedContent: 'p br h2 ul ol li strong em'
          }
        },
        allowedContent: 'div(!wysiwyg-content); div(!wysiwyg-content__text)',
        requiredContent: 'div(wysiwyg-content)',
        upcast: function (element) {
          return element.name === 'div' && element.hasClass('wysiwyg-content--double-text');
        }
      });

      if (editor.ui.addButton) {
        editor.ui.addButton('imageteaserleft', {
          label: 'Image teaser (left) new',
          command: 'imageteaserleft',
          icon: this.path + 'icons/imageteaserleft.png'
        });
        editor.ui.addButton('imageteaserright', {
          label: 'Image teaser (right) new',
          command: 'imageteaserright',
          icon: this.path + 'icons/imageteaserright.png'
        });
        editor.ui.addButton('imageteaserrightfull', {
          label: 'Image teaser full (right) new',
          command: 'imageteaserrightfull',
          icon: this.path + 'icons/imageteaserrightfull.png'
        });
        editor.ui.addButton('doubleimageblock', {
          label: 'Double image block new',
          command: 'doubleimageblock',
          icon: this.path + 'icons/doubleimageblock.png'
        });
        editor.ui.addButton('doubletextblock', {
          label: 'Double text block new',
          command: 'doubletextblock',
          icon: this.path + 'icons/doubletextblock.png'
        });
      }
    }
  });

})(jQuery, Drupal, CKEDITOR);
