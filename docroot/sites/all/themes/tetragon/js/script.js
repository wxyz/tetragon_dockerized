jQuery(window).load(function() {



    var stickyNavTop = jQuery('.navbar-default').offset().top;

    var stickyNav = function(){
        var scrollTop = jQuery(window).scrollTop();

        if (scrollTop > stickyNavTop) {
            jQuery('.navbar-default').addClass('sticky');
        } else {
            jQuery('.navbar-default').removeClass('sticky');
        }
    };



    jQuery('.view-company-page-slideshow .view-content').owlCarousel({
        loop:true,
        margin:0,
        items:1,
        nav:true,
        navText:['<span class="glyphicon  glyphicon-chevron-left" aria-hidden="true"></span>','<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:2
            }
        }
    })

    jQuery(window).scroll(function() {
        // console.log(jQuery(window).width());
        if(jQuery(window).width() > 1040)
            stickyNav();
    });

    jQuery('.view-frontpage-slideshow .view-content').owlCarousel({
        items:1,
        loop:true,
        nav: true,
        navText:['<span class="glyphicon  glyphicon-chevron-left" aria-hidden="true"></span>','<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'],
        dots:true
    });


    var owl = jQuery('.view-id-services_slideshow.view-display-id-block .view-content');
    owl.owlCarousel({
        items:1,
        loop:false
    });
// Listen to owl events:

    if (jQuery('.view-id-services_slideshow.view-display-id-block .view-content').length > 0){
        change_active_title();
    }

    owl.on('changed.owl.carousel', function(event) {

        change_active_title();

    });

    jQuery('.view-display-id-block_1 .views-field.views-field-title').click( function () {
        // console.log('clicked');
        node = jQuery(this).find('.nid-content').attr('data-nid');


        jQuery(".view-display-id-block_1 .nid-content").parent().removeClass('active');
        jQuery(this).find('.field-content').addClass('active');

        slides=jQuery(".view-display-id-block .owl-item");
        console.log(slides );

        jQuery.each(slides, function( index, value ) {
            if (jQuery(this).find('#'+node).length > 0 ) {
                owl.trigger('to.owl.carousel', index);
                return false;
            }

            //         console.log(index);
            // selector="#"+node;
            // find=jQuery(this).find(selector);
            // console.log(find);
        });


    });


//     jQuery('.view-id-services_slideshow.view-display-id-block .view-content').owlCarousel({
//         items:1,
//         loop:true
//     })

    /* mobile menu */
    jQuery('.menu-wrapper.mobile button').click(function(){
        jQuery('.my-navbar-collapse').toggle();
    });

    jQuery('.view.view-services-slideshow .nav').wrap("<div class='nav-wrapper'></div>");


    // w_height=jQuery(window).height();
    //
    // console.log(w_height);
    // // jQuery('.window-height').height(w_height);
    // jQuery('.window-height .owl-stage-outer').height(w_height);

});

function change_active_title() {
    setTimeout( function () {
        current=jQuery(".owl-item.active .tab-pane");

        node=jQuery(current[0]).attr('id');

        jQuery(".view-display-id-block_1 .nid-content").parent().removeClass('active');

        el=jQuery(".view-display-id-block_1 [data-nid="+node+"]").parent().addClass('active');
    }, 300 );
}