<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
    $prim_nav= '<div class="prim-nav"></div>';
    $sec_nav= '<div class="sec-nav"></div>';

if (!empty($primary_nav))
    $prim_nav= render($primary_nav);
if (!empty($secondary_nav))
    $sec_nav= render($secondary_nav);

?>
<header id="navbar" role="banner" class="navbar-default ">

    <div class="container">


        <div class="row">



            <div class="col-lg-3 col-md-3 col-sm-12 image-wrapper">
                <?php if ($logo): ?>
                    <a class="logo navbar-btn" href="<?php print $front_page; ?>"
                       title="<?php print t('Home'); ?>">
                        <img src="<?php print $logo; ?>" class='img-responsive white' alt="<?php print t('Home'); ?>"/>
                        <img src="/sites/default/files/logo-grey.png" class="grey img-responsive">
                    </a>
                <?php endif; ?>
            </div>

            <!--      --><?php //if (!empty($site_name)): ?>
            <!--        <a class="name navbar-brand" href="--><?php //print $front_page; ?><!--" title="-->
            <?php //print t('Home'); ?><!--">--><?php //print $site_name; ?><!--</a>-->
            <!--      --><?php //endif; ?>



            <div class="col-lg-7 col-md-7 col-sm-12  menu-wrapper desktop">
                <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>

                    <?php print $prim_nav; ?>

                    <?php print $sec_nav; ?>

                <?php endif; ?>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-12  menu-wrapper desktop lang">
                <?php
                $block = module_invoke('locale', 'block_view', 'language');
                print $block['content']; ?>
            </div>


            <div class="menu-wrapper mobile col-xs-12">
                <button type="button" class="" data-toggle="collapse" data-target=".my-navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <div class="my-navbar-collapse">
                    <?php if (!empty($primary_nav) || !empty($secondary_nav) ): ?>

                        <?php print $prim_nav; ?>

                        <?php print $sec_nav; ?>

                    <?php endif; ?>

                    <?php print $block['content']; ?>
                </div>


            </div>
        </div>

    </div>

</header>

<div class="main-container">

    <header role="banner" id="page-header">
        <?php if (!empty($site_slogan)): ?>
            <p class="lead"><?php print $site_slogan; ?></p>
        <?php endif; ?>

        <?php print render($page['header']); ?>
    </header> <!-- /#page-header -->


    <section>
    <?php print $messages; ?>
        <?php print render($page['content']); ?>
    </section>


</div>

<?php if (!empty($page['footer'])): ?>
    <div class="footer">
        <footer class=" container">
            <div class="row">
                <div class="col-xs-3 col-md-2 footer--logo">
                    <!-- <img src="/sites/default/files/TETRAGON-WHITE_1.png" class="img-responsive"> -->
                    <img src="/sites/all/themes/tetragon/images/logo-footer.png" class="img-responsive">

                    <div class="footer--certifications">
                        <img src="/sites/default/files/OSPI.png"     class='img-responsive'>
                        <img src="/sites/default/files/ecsite.png"   class='img-responsive'>
                        <img src="/sites/default/files/ISO-logo.png" class='img-responsive'>
                    </div>
                </div>

                <div class="mobile-border col-md-10 col-xs-9">

                    <div class="footer-border-wrapper">
                        <div class="row">
                            <div class="col-md-2 footer--images">
                                <div class="footer--certifications">
                                    <img src="/sites/default/files/OSPI.png"     class='img-responsive'>
                                    <img src="/sites/default/files/ecsite.png"   class='img-responsive'>
                                    <img src="/sites/default/files/ISO-logo.png" class='img-responsive'>
                                </div>
                            </div>

                            <div class=" col-xs-12 col-md-4 footer--form desktop">
                                <div class="newsletter-wrapper">
                                    <div class="footer--subtitle">
                                        <?php print t('Newsletter'); ?>
                                    </div>
                                    <div class="footer--subtitle-description">
                                        <?php print t('If you want to receive information about the
                                        company and our news, fill in your email bellow.'); ?>
                                    </div>

                                    <?php
                                        $block = module_invoke('webform', 'block_view', 'client-block-41');
                                        print render($block['content']);
                                    ?>
                                    </div>
                            </div>

                            <div class="col-xs-6 col-sm-4 col-md-2 col-md-2 footer--links">

                                <div class="footer-links-wrapper">
                                    <div class="footer--subtitle">
                                        <?php print t('Links'); ?>
                                    </div>
                                    <div class="footer--menu">
                                        <?php print $prim_nav; ?>
                                    </div>
                                </div>


                            </div>

                            <div class=" col-xs-10 col-sm-8 col-md-4 footer--contact">
                                <div class="footer--subtitle">
                                      <?php print t('Contact'); ?>
                                </div>
                                <?php print render($page['footer']); ?>
                            </div>

                        </div>

                        <div class="row">
                            <div class=" col-xs-6 col-md-4 footer--form mobile">
                                <div class="footer--subtitle">
                                    <?php print t('Newsletter'); ?>
                                </div>
                                <div class="footer--subtitle-description">
                                    <?php print t('If you want to receive information about the
                                        companyand our news, fill in your email bellow.'); ?>
                                </div>
                                <!--                            <div class="footer--email-label">-->
                                <!---->
                                <!--                            </div>-->
                                <?php
                                    $block = module_invoke('webform', 'block_view', 'client-block-41');
                                    print render($block['content']);
                                ?>

                                <!--                            <div class="footer--button">-->
                                <!--                                <a href="#">--><?php //print t('Subscription'); ?><!--</a>-->
                                <!--                            </div>-->
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </footer>
    </div>
<?php endif; ?>
