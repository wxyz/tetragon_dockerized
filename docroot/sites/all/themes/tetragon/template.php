<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

function tetragon_preprocess_html(&$variables)
{

    drupal_add_css("https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&subset=latin,greek", array('type'=>'external'));

	$white_nav_nodes = array(48,47,46,43,40,45,44,3,3,42,45,49,61);

    if(arg(0)=='node' && is_numeric(arg(1))) {
    	if (in_array(arg(1), $white_nav_nodes)){
    		$variables['classes_array'][] = 'white-nav';
    	} else { $node = node_load(arg(1)); if ($node->type=='project') { $variables['classes_array'][]='white-nav'; } }

        //$node = node_load(arg(1));
        //$results = taxonomy_node_get_terms($node);
       //  if(is_array($results)) {
       //      foreach ($results as $item) {
       //         $variables['classes_array'][] = "taxonomy-".strtolower(drupal_clean_css_identifier($item->name));
       //      }
       // }
   }
}

function tetragon_preprocess_node ( &$vars ) {
    if ($vars["is_front"]) {
        $vars["theme_hook_suggestions"][] = "node__front";
    }
}
